#pragma once

// Variable
const int RCVBUFSIZE = 32;
const int TCPPORT = 23;

// Protocol Type Command
const int MODE_DEFAULT          = 0;
const int MODE_CONNECT          = 1;
const int MODE_DISCONNECT       = 2;
const int MODE_ENABLE           = 3;
const int MODE_UNABLE           = 4;
const int MODE_RESET            = 5;
const int MODE_SET_DIRECTION    = 6;
const int MODE_DELETE_DIRECTION = 7;
const int MODE_SET_NONE         = 8;
const int MODE_SET_VIEW         = 9;
const int MODE_SET_VIEWCONTROL  = 10;

// Protocol Type Return
const int RET_OK                = 0;
const int RET_SAMEDIRECTION     = 1;




// Viewer Packet data
typedef struct TCPPacket
{
	int nMode;
    int nRet;
	char strIp[17];
}TCPPacket;

typedef struct PCData
{
    int nState;
    int nDirection;
}PCData;
