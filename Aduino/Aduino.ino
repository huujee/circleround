// NeoPixel Ring simple sketch (c) 2013 Shae Erisson
// released under the GPLv3 license to match the rest of the AdaFruit NeoPixel library

#include <Adafruit_NeoPixel.h>

#define PIN_IN_A        2
#define PIN_IN_B        4
#define PIN_LED         6
#define NUMPIXELS      24             // LED Count

enum {MODE_STANDBY, MODE_CONNECTING, MODE_ALL, MODE_SETTING, MODE_ERROR};

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_LED, NEO_GRB + NEO_KHZ800);

int nState = MODE_STANDBY;
int delayval = 500;                 // delay for half a second

void setup() {
  pinMode(PIN_IN_A, INPUT);
  pinMode(PIN_IN_B, INPUT);
  pixels.begin();                   // This initializes the NeoPixel library.
}

void setColor(int r, int g, int b) {
  for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i, pixels.Color(r,g,b));
  }

  pixels.show();                                        // This sends the updated pixel color to the hardware.
  delay(delayval);                                      // Delay for a period of time (in milliseconds).
}

void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*3; j++) {                                  // 3 cycles of all colors on wheel
    for(i=0; i< NUMPIXELS; i++) {
      pixels.setPixelColor(i, Wheel(((i * 256 / NUMPIXELS) + j) & 255));
    }
    pixels.show();
    delay(wait);
  }
}

void loop() {

  int read1 = digitalRead(PIN_IN_A);
  int read2 = digitalRead(PIN_IN_B);

  // Analyze Read Data
  if(read1 == LOW)
  {
    nState = (read2 == LOW) ? MODE_STANDBY : MODE_CONNECTING;
  }
  else
  {
    nState = (read2 == LOW) ? MODE_SETTING : MODE_ALL;
  }

  // Show Analyzed Data
  switch(nState)
  {
  case MODE_STANDBY:
    setColor(100, 0, 0);
    break;
  case MODE_CONNECTING:
    setColor(100, 50, 0);
    break;
  case MODE_SETTING:
    rainbowCycle(5);
    break;
  case MODE_ALL:
    setColor(0, 100, 0);
    break;
  }

}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
