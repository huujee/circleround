/*
 *   C++ sockets on Unix and Windows
 *   Copyright (C) 2002
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "PracticalSocket.h"  // For Socket, ServerSocket, and SocketException
#include "CommonDevice.h"
#include "wiringPi.h"
#include <iostream>           // For cout, cerr
#include <cstdlib>            // For atoi()
#include <pthread.h>          // For POSIX threads
#include <string.h>
#include <map>
#include <softPwm.h>

using namespace std;

// Stacked Encoders (2 pins per encoder)
#define max_encoders 2

// Define using GPIO
#define PIN_LED_A           0
#define PIN_LED_B           1
#define PIN_ENCODER_BTN     2
#define PIN_ENCODER_1A      5
#define PIN_ENCODER_1B      6
#define PIN_ENCODER_2A      3
#define PIN_ENCODER_2B      4


struct encoder
{
    int pin_a;
    int pin_b;
    volatile long value;
    volatile int lastEncoded;
};


enum {    MODE_STANDBY, MODE_CONNECTING, MODE_ALL, MODE_SETTING, MODE_ERROR   };


/* Global Variables */

//Pre-allocate encoder objects on the stack so we don't have to
//worry about freeing them
struct encoder encoders[max_encoders];
int numberofencoders = 0;

// Control
int shortValue = 0;
TCPSocket* currentShortSocket = NULL;
long preShortValue = 0;
// View
int longValue = 0;
TCPSocket* currentLongSocket = NULL;
long preLongValue = 0;

std::map<TCPSocket*, PCData*> clientSocket;

// Current Mode
int nCurrentMode = MODE_STANDBY;


/* Global Methods */

void* ThreadAccepted(void *arg);                // Main program of a thread
void *ThreadEncoder(void *arg);                 // Encoder Handling
void* ThreadControl(void *arg);                 // Control Arduino Pro Mini
//void *ThreadLed(void *arg);                   // Led Handling

void updateEncoders();
void setupControl(int pin_a, int pin_b);
struct encoder *setupEncoder(int pin_a, int pin_b);
void setupButton(int pin_a);
void clickedButton();
void clickedReset();

void SendMessage(TCPSocket* pSocket, int nMode, int nRet = 0, string strIp = "NONE")
{
    TCPPacket clntPacket;
    clntPacket.nMode   = nMode;
    clntPacket.nRet    = nRet;

    if (strIp != "NONE")
    {
        strcpy(clntPacket.strIp, strIp.c_str());
    }

    pSocket->send(&clntPacket, sizeof(TCPPacket));

    cout << "[SendMessage] Mode : " << nMode << ", Send to : " << pSocket->getForeignAddress() << ", IP : " << strIp << endl;
}

int main()
{
    cout << "[main] Main Started" << endl;

    wiringPiSetup () ;

    pthread_t controlThreadID;              // Thread ID from pthread_create()
    if (pthread_create(&controlThreadID, NULL, ThreadControl, NULL) != 0)
    {
      cerr << "[main] Unable to create Control thread" << endl;
    }
    else
    {
        cout << "[main] Created Control thread" << endl;
    }

    pthread_t encoderThreadID;              // Thread ID from pthread_create()
    if (pthread_create(&encoderThreadID, NULL, ThreadEncoder, NULL) != 0)
    {
      cerr << "[main] Unable to create Encoder thread" << endl;
    }
    else
    {
        cout << "[main] Created Encoder thread" << endl;
    }

    try
    {
        TCPServerSocket servSock(TCPPORT);   // Socket descriptor for server

        while (true)
        {
            cout << "[main] Waiting a client" << endl;

            // Create separate memory for client argument
            TCPSocket *clntSock = servSock.accept();

            // Create client thread
            pthread_t threadID;              // Thread ID from pthread_create()
            if (pthread_create(&threadID, NULL, ThreadAccepted, (void *) clntSock) != 0)
            {
                cerr << "[main] Unable to create TCP thread" << endl;
            }
            else
            {
                cout << "[main] Created TCP Thread" << endl;

                //TCPPacket* clntPacket = new TCPPacket();
                PCData* pcData = new PCData();
                pcData->nState = MODE_CONNECT;
                pcData->nDirection = -1;

                clientSocket.insert(std::map<TCPSocket*, PCData*>::value_type(clntSock, pcData));

                if(nCurrentMode == MODE_STANDBY)
                {
                    nCurrentMode = MODE_CONNECTING;
                    cout << "[main] MODE CONNECTING" << endl;
                }
            }
        }
    }
    catch (SocketException &e)
    {
        cerr << e.what() << endl;
        exit(1);
    }
    // NOT REACHED

    return 0;
}

void updateEncoders()
{
    struct encoder *encoder1= encoders;
    for (int i = 0 ; i < max_encoders; i++)
    {
        int MSB = digitalRead(encoder1->pin_a);
        int LSB = digitalRead(encoder1->pin_b);

        int encoded = (MSB << 1) | LSB;
        int sum = (encoder1->lastEncoded << 2) | encoded;

        if(sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) encoder1->value++;
        if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) encoder1->value--;

        encoder1->lastEncoded = encoded;

        encoder1++;
    }
}

void setupControl(int pin_a, int pin_b)
{
    pinMode(pin_a, OUTPUT);
    pinMode(pin_b, OUTPUT);

    digitalWrite(pin_a, LOW);
    digitalWrite(pin_b, LOW);
}

struct encoder *setupEncoder(int pin_a, int pin_b)
{
    if (numberofencoders > max_encoders)
    {
        cout << "Maximum number of encodered exceded: " << max_encoders << endl;
        return NULL;
    }

    struct encoder *newencoder = encoders + numberofencoders++;
    newencoder->pin_a = pin_a;
    newencoder->pin_b = pin_b;
    newencoder->value = 0;
    newencoder->lastEncoded = 0;

    pinMode(pin_a, INPUT);
    pinMode(pin_b, INPUT);
    pullUpDnControl(pin_a, PUD_UP);
    pullUpDnControl(pin_b, PUD_UP);
    wiringPiISR(pin_a,INT_EDGE_BOTH, updateEncoders);
    wiringPiISR(pin_b,INT_EDGE_BOTH, updateEncoders);

    return newencoder;
}

void setupButton(int pin_a)
{
    pinMode(pin_a, INPUT);
    pullUpDnControl(pin_a, PUD_UP);
}

void SetDirection(TCPSocket *pSocket, PCData* pData, bool bSet)
{
    int val = (int)longValue;

    if(bSet == true)
    {
        std::map<TCPSocket*, PCData*>::iterator iter;

        for( iter = clientSocket.begin() ; iter != clientSocket.end() ; iter++)
        {
            if ( (val == iter->second->nDirection) && (iter->second->nDirection != -1) )
            {
                SendMessage(pSocket, MODE_SET_DIRECTION, RET_SAMEDIRECTION);

                cout << "[SetDirection] Error SetDirection : Same Direction" << endl;
                return;
            }
        }

        //if (pData->nDirection == -1) // for re-direction
        //{
            pData->nDirection = val;

            cout << "[SetDirection] nDirection : " << val << endl;
        //}
    }
    else
    {
        pData->nDirection = -1;
    }
}

void clickedButton()
{
    int nShortSelected = 16;
    int nShortSelectedDirection = 0;
    int nLongSelected = 16;
    int nLongSelectedDirection = 0;
    TCPSocket* pShortSocket = NULL;
    TCPSocket* pLongSocket = NULL;

    std::map<TCPSocket*, PCData*>::iterator iter;

    for( iter = clientSocket.begin() ; iter != clientSocket.end() ; iter++)
    {
        int nShortDistance = shortValue - iter->second->nDirection;
        int nLongDistance = longValue - iter->second->nDirection;

        if(nShortDistance < 0)
        {
            nShortDistance = nShortDistance * -1;
        }
        if(nShortDistance > 7)
        {
            nShortDistance = 16-nShortDistance;
        }

        if(nLongDistance < 0)
        {
            nLongDistance = nLongDistance * -1;
        }
        if(nLongDistance > 7)
        {
            nLongDistance = 16-nLongDistance;
        }

        if(nShortDistance < nShortSelected)
        {
            pShortSocket = iter->first;
            nShortSelected = nShortDistance;
            nShortSelectedDirection = iter->second->nDirection;
        }
        if(nLongDistance < nLongSelected)
        {
            pLongSocket = iter->first;
            nLongSelected = nLongDistance;
            nLongSelectedDirection = iter->second->nDirection;
        }
    }

    cout << "[clickedButton] Closest Control : " << nShortSelectedDirection << ", Distance : " << nShortSelected << endl;
    cout << "[clickedButton] Closest View : " << nLongSelectedDirection << ", Distance : " << nLongSelected << endl;

    for( iter = clientSocket.begin() ; iter != clientSocket.end() ; iter++)
    {

        if (iter->first != pLongSocket)
        {
            if (iter->first == pShortSocket)
            {
                SendMessage(iter->first, MODE_SET_VIEWCONTROL, RET_OK, pLongSocket->getForeignAddress());
            }
            else
            {
                SendMessage(iter->first, MODE_SET_VIEW, RET_OK, pLongSocket->getForeignAddress());
            }
        }
        else
        {
            SendMessage(iter->first, MODE_SET_NONE, RET_OK);
        }
    }

    currentShortSocket = pShortSocket;
    currentLongSocket = pLongSocket;

    nCurrentMode = MODE_SETTING;
    cout << "[clickedButton] MODE SETTING" << endl;

    // LED Test
    //else if (nCurrentMode == MODE_SETTING)
    //    nCurrentMode = MODE_STANDBY;
    //else if (nCurrentMode == MODE_ALL)
    //    nCurrentMode = MODE_STANDBY;
    //else if (nCurrentMode == MODE_STANDBY)
    //    nCurrentMode = MODE_CONNECTING;
}

void clickedReset()
{
    std::map<TCPSocket*, PCData*>::iterator iter;

    for( iter = clientSocket.begin() ; iter != clientSocket.end() ; iter++)
    {
        try {
            SendMessage(iter->first, MODE_SET_NONE, RET_OK);
        }
        catch(...) {
            cerr << "[clickedReset] exception : send SET_NONE" << endl;
        }
    }

    delay(1000);

    for( iter = clientSocket.begin() ; iter != clientSocket.end() ; iter++)
    {
        try {
            if(iter->first != NULL)
                delete (iter->first);
            if(iter->second != NULL)
                delete (iter->second);
            clientSocket.erase(iter);
        }
        catch(...) {
            cerr << "[clickedReset] exception : delete" << endl;
        }
    }

    nCurrentMode = MODE_STANDBY;

    cout << "[clickedReset] initialized " << endl;
}


void *ThreadControl(void *argData)
{
    // Guarantees that thread resources are deallocated upon return
    pthread_detach(pthread_self());

    setupControl(PIN_LED_A, PIN_LED_B);

    try
    {
        while(true)
        {
            switch(nCurrentMode)
            {
            case MODE_STANDBY:
                digitalWrite(PIN_LED_A, LOW);
                digitalWrite(PIN_LED_B, LOW);
                break;
            case MODE_CONNECTING:
                digitalWrite(PIN_LED_A, LOW);
                digitalWrite(PIN_LED_B, HIGH);
                break;
            case MODE_SETTING:
                digitalWrite(PIN_LED_A, HIGH);
                digitalWrite(PIN_LED_B, LOW);
                delay(1000);
                nCurrentMode = MODE_ALL;
                cout << "[ThreadControl] MODE ALL" << endl;
                break;
            case MODE_ALL:
                digitalWrite(PIN_LED_A, HIGH);
                digitalWrite(PIN_LED_B, HIGH);
                break;
            }
            delay(100);
        }
    }
    catch(...)
    {
        cerr << "[ThreadControl] exception" << endl;
    }

    cout << "[ThreadControl] Terminating " << endl;
    return NULL;
}

/*
// LED Handling
void *ThreadLed(void *argData) {
    // Guarantees that thread resources are deallocated upon return
    pthread_detach(pthread_self());

    try
    {
        ws2812b *_ws2812b = new ws2812b(1); //1 pixel LED
        _ws2812b->initHardware();
        _ws2812b->clearLEDBuffer();

        for(int k = 0 ; k < 1 ; k++) {

          _ws2812b->setPixelColor(k, 255, 255, 255);
          _ws2812b->show();
        }

        delete _ws2812b;
    }
    catch(...)
    {
        cerr << "[ThreadLed] exception" << endl;
    }
    */

/*
  while(true) {
      switch(nCurrentMode) {
      case MODE_STANDBY:
          {
              for(int i = 3 ; i <= 10 ; i++) {
                  _ws2812b->setPixelColor(0, i, 0, 0);
                  _ws2812b->show();
                  usleep(100000);
              }
              for(int i = 9 ; i > 2 ; i--) {
                  _ws2812b->setPixelColor(0, i, 0, 0);
                  _ws2812b->show();
                  usleep(100000);
              }
          }

          nCurrentMode = MODE_CONNECTING;

          break;
      case MODE_CONNECTING:
          {
              for(int i = 3 ; i <= 10 ; i++) {
                  _ws2812b->setPixelColor(0, i, i, 0);
                  _ws2812b->show();
                  usleep(100000);
              }
              for(int i = 9 ; i > 2 ; i--) {
                  _ws2812b->setPixelColor(0, i, i, 0);
                  _ws2812b->show();
                  usleep(100000);
              }
          }

          nCurrentMode = MODE_ALL;

          break;
      case MODE_ALL:
          _ws2812b->setPixelColor(0, 0, 0, 6);
          _ws2812b->show();

          nCurrentMode = MODE_PRIVATE;

          break;
      case MODE_PRIVATE:
          _ws2812b->setPixelColor(0, 0, 6, 0);
          _ws2812b->show();

          nCurrentMode = MODE_STANDBY;

          break;
      case MODE_ERROR:
          break;
      default:
          break;
      }
      usleep(1000000);
  }
*/

  /*
  int tmp;

  for(;;){
      //RGB Blink.
      _ws2812b->setPixelColor(0, 255, 0, 0);
      _ws2812b->show();
      usleep(1000);

      _ws2812b->setPixelColor(0, 0, 255, 0);
      _ws2812b->show();
      usleep(1000);

      _ws2812b->setPixelColor(0, 0, 0, 255);
      _ws2812b->show();
      usleep(1000);

      //Rainbow
      for( int i=0 ; i<=255 ; i++){
          if( i < 85 ){
              _ws2812b->setPixelColor(0, i*3, 255-i*3, 0);
              _ws2812b->show();
          }else if( i < 170 ){
              tmp = i-85;
              _ws2812b->setPixelColor(0, 255-tmp*3, 0, tmp*3);
              _ws2812b->show();
          }else{
              tmp = i-170;
              _ws2812b->setPixelColor(0, 0, tmp*3, 255-tmp*3);
              _ws2812b->show();
          }
          usleep(1);
      }

      usleep(1000);

      cout << "[ThreadLed] loop" << endl;
  }
  */
/*
  cout << "[ThreadLed] Terminating " << endl;
  return NULL;
}
*/

// Encoder Handling
void *ThreadEncoder(void *argData) {
    // Guarantees that thread resources are deallocated upon return
    pthread_detach(pthread_self());

    // using pins 1 (GPIO 27)
    setupButton(PIN_ENCODER_BTN);
    // using pins 3/4 (GPIO 22/23)
    struct encoder *encoder1 = setupEncoder(PIN_ENCODER_1A,PIN_ENCODER_1B);
    // using pins 5/6 (GPIO 24/25)
    struct encoder *encoder2 = setupEncoder(PIN_ENCODER_2A,PIN_ENCODER_2B);

    while(1)
    {
        updateEncoders();

        // Short Side Encoder
        long s = encoder1->value;
        if(s!=preShortValue)
        {
            preShortValue = s;

            if(s > 0)
            {
              shortValue = (int)((s/4)%16);
            }
            else
            {
              shortValue = (int)(15 - ((s * -1)/4)%16);
            }

            cout << "[ThreadEncoder] Short Value: " << s << ", " << shortValue << endl;
        }

        // Long Side Encoder
        long l = encoder2->value;
        if(l!=preLongValue)
        {
            preLongValue = l;

            if(l > 0)
            {
              longValue = (int)((l/4)%16);
            }
            else
            {
              longValue = (int)(15 - ((l * -1)/4)%16);
            }

            cout << "[ThreadEncoder] Long Value: " << l << ", " << longValue << endl;
        }

        // Button
        if(digitalRead(PIN_ENCODER_BTN) == LOW)
        {
            cout << "[ThreadEncoder] Pushed Short Button Down" << endl;
            delay(500);

            if(digitalRead(PIN_ENCODER_BTN) == HIGH)
            {
                 clickedButton();
                 cout << "[ThreadEncoder] Pushed Short Button Up" << endl;
            }
            else
            {
                delay(500);

                if(digitalRead(PIN_ENCODER_BTN) == LOW)
                {
                    while(digitalRead(PIN_ENCODER_BTN) == LOW)
                    {
                        delay(100);
                    }
                    clickedReset();
                    delay(3000);
                }
            }
        }
        /*
        // Button
        if(digitalRead(PIN_ENCODER_BTN) == LOW)
        {
         delay(1000);

         if(digitalRead(PIN_ENCODER_BTN) == HIGH) {

             delay(500);

             if(digitalRead(PIN_ENCODER_BTN) == LOW) {
                 clickedDoubleButton();
             }
             else {
                 clickedShortButton();
                 cout << "[ThreadEncoder] Pushed Short Button" << endl;
             }
         }
         else {
             delay(2000);

             if(digitalRead(PIN_ENCODER_BTN) == LOW) {
                 clickedLongButton();
             }
         }

         delay(1000);
        }
        */
        delay(50);
    }


    cout << "[ThreadAccepted] Terminating " << endl;
    return NULL;
}


// Accepted Client Handling
void *ThreadAccepted(void *clntSock) {
    // Guarantees that thread resources are deallocated upon return
    pthread_detach(pthread_self());

    TCPSocket *sock = (TCPSocket *) clntSock;

    cout << "[ThreadAccepted] Handling client ";
    try {
        cout << sock->getForeignAddress() << ":";
    } catch (SocketException &e) {
        cerr << "[ThreadAccepted] Unable to get foreign address" << endl;
    }

    try {
        cout << sock->getForeignPort() << endl;
    } catch (SocketException &e) {
        cerr << "[ThreadAccepted] Unable to get foreign port" << endl;
    }
    cout << "[ThreadAccepted] with thread " << pthread_self() << endl;

    int recvMsgSize;
    try
    {
        // Send received string and receive again until the end of transmission
        TCPPacket pPacket;
        while ((recvMsgSize = sock->recv(&pPacket, sizeof(TCPPacket))) > 0)
        {
          switch(pPacket.nMode)
          {
          case MODE_SET_DIRECTION:
              SetDirection(sock, clientSocket[sock], true);
              break;
          case MODE_DELETE_DIRECTION:
              SetDirection(sock, clientSocket[sock], false);
              break;
          default:
              break;

          }
        }
    }
    catch(SocketException &e)
    {
        cerr << "[ThreadAccepted] recv exception : " << e.what() << endl;
    }

    //////////// Must modify!! Don't do this in a separated thread ///////////////////////////////////////
    std::map<TCPSocket*, PCData*>::iterator iter = clientSocket.begin();

    for(;iter != clientSocket.end();iter++)
    {
        if (iter->first == clntSock)
        {
          delete (iter->first);
          delete (iter->second);
          clientSocket.erase(iter);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////

    //delete (TCPSocket *) clntSock;

    cout << "[ThreadAccepted] Terminating " << endl;
    return NULL;
}
